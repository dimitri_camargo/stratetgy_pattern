/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package strategypattern;

/**
 *
 * @author aluno.redes
 */
public class Bird extends Animal_Context{
    public Bird(){
        super();
        super.setSound("tweet");
        //setting the flying 
        super.flyingType = new FlyWithWings_ConcreteStrategyB();
    }
}
