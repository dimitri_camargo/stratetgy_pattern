/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package strategypattern;

/**
 *
 * @author aluno.redes
 */
public class SuperDog extends Dog{
    public SuperDog(){
        super();
        
        super.flyingType = new FlyWithRockets();
    }
}
