/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package strategypattern;

/**
 *
 * @author aluno.redes
 */
public class StrategyPattern {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Animal_Context scooby = new Dog();
        Animal_Context krypton = new SuperDog();
        Animal_Context woodPecker = new Bird();
        
        System.out.println("scooby try to fly"+scooby.tryToFly());
        
        System.out.println("krypton try to fly: "+krypton.tryToFly());
        
        System.out.println("woodpecker try to fly: "+woodPecker.tryToFly());
        
        System.out.println("woodpecker got a salt in the Tail!");
        
        woodPecker.setFlyBehavior(new FlyNoWay_ConcreteStrategyA());
        
        System.out.println("woodpecker try to fly: "+woodPecker.tryToFly());
    }
    
}
