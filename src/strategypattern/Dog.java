/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package strategypattern;

/**
 *
 * @author aluno.redes
 */
public class Dog extends Animal_Context{
    public Dog(){
        super();
        super.setSound("woof woof!");
        
        //setting the flying algorithm to dog
        super.flyingType = new FlyNoWay_ConcreteStrategyA();
    }
    
    public void digHole(){
        System.out.println("digging a hole");
    }
}
