/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package strategypattern;

/**
 *
 * @author aluno.redes
 */                         //não implementa segundo o giovan
public class Animal_Context /*implements FlyStrategy_Strategy */{
    private String name;
    private int weight;
    private double speed;
    private String sound;
    //using variable of FlyStrategy_Strategy interface
    protected FlyStrategy_Strategy flyingType;
    
    public Animal_Context(){
        
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }

    public double getSpeed() {
        return speed;
    }

    public void setSpeed(double speed) {
        this.speed = speed;
    }

    public String getSound() {
        return sound;
    }

    public void setSound(String sound) {
        this.sound = sound;
    }

    public FlyStrategy_Strategy getFlyType() {
        return flyingType;
    }

    public void setFlyType(FlyStrategy_Strategy flyType) {
        this.flyingType = flyType;
    }
    
    public String tryToFly(){
        //Animal set responsability for flying to flyingType
        return this.flyingType.fly();
    }
    
    //methos to change the flyingType dynamically
    public void setFlyBehavior(FlyStrategy_Strategy newFlyingType){
        this.flyingType = newFlyingType;
    }
    
   /* @Override
    public String fly() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }*/
    
    
    
}
