/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package strategypattern;

/**
 *
 * @author aluno.redes
 */
public class RubberBird extends Bird{
    public RubberBird(){
        super();
        super.setSound("no sound");
        
        super.flyingType = new FlyNoWay_ConcreteStrategyA();
    }
    
    
}
