/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package strategypattern;

/**
 *
 * @author aluno.redes
 */
public class FlyWithWings_ConcreteStrategyB implements FlyStrategy_Strategy{

    @Override
    public String fly() {
        return "i´m using my wings to fly";
    }
    
}
